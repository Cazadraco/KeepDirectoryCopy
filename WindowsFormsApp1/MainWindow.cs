﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic.FileIO;
using System.Threading;
using System.IO;
namespace WindowsFormsApp1
{
    public partial class MainWindow : Form
    {
        string sourceFolder = @"C:\ARKServerManager\data\servers\_backup\aberrationserver\";
        string outputFolder = @"E:\BACKUPSARK\";
        string path = @"E:\BACKUPSARK\History.txt"; //Archivo TXT
        double TiempoRefresh = 3600; //Segundos
        int TiempoCheck = 600; //Segundos
        DateTime DT;
        DateTime DTplusRefreshTime;
        public MainWindow()
        {
            InitializeComponent();
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {
            this.Hide();
            CopiarDirectorio();
            resetTime();
            while (true) { 
            if (DateTime.Now >= DTplusRefreshTime) {
                CopiarDirectorio();
                resetTime();
                }
                Thread.Sleep(TiempoCheck * 1000);
            }
            
        }

        private void resetTime() {
            DT = DateTime.Now;
            DTplusRefreshTime = DT.AddSeconds(TiempoRefresh);
        }

        private void CopiarDirectorio() {
            var directorySource = new DirectoryInfo(sourceFolder);
            var myFileSource = (from f in directorySource.GetFiles()
                          orderby f.LastWriteTime descending
                          select f).First();
            var directoryOutput = new DirectoryInfo(outputFolder);
            var myFileOutput = (from f in directoryOutput.GetFiles()
                          orderby f.LastAccessTime descending
                          select f).First();

            if (myFileSource.LastWriteTime > myFileOutput.LastAccessTime) { 

            FileSystem.CopyDirectory(sourceFolder, outputFolder, false);
           

          //  AddTXTLine();
            }
        }
        private void AddTXTLine()
        {
           
            if (!File.Exists(path))
            {
                File.Create(path);
                using (var tw = new StreamWriter(path, true))
                {
                    tw.WriteLine(DateTime.Now.ToString());
                    tw.Close();
                    
                }

            }
            else if (File.Exists(path))
            {
                using (var tw = new StreamWriter(path, true))
                {
                    tw.WriteLine(DateTime.Now.ToString());
                    tw.Close();
                }
            }


        }
    }
}
